# Weebot

*Not another music player*

Weebot is a bot for the social chat platform
[Discord](https://discordapp.com/).

Weebot is still under construction, but you can see a
[Feature List](#FeatureList) below as well as the current [Roadmap](#Roadmap).
Go to the [ChangeLogs](#Log) to see the most recent changes in development
progress.

Join Weebot's home server, NL, for live updates https://discord.gg/yj9bhjY .

----
<a name='FeatureList'></a>
**Feature List**
- Settings
    - Set bot nickname.
    - Change the call-sign (or prefix) used to call the bot directly.
    - Allow or disallow the bot to use explicit language.
    - Allow or disallow the bot to use NSFW commands.
    - Server-wide word bans. (Under construction)
    - Allow or disallow the bot to respond to actions not directed to it.
- Commands
    - Utility
        - Note Pads
            - Write and edit Note Pads to keep track of ideas and plans.
            - Lock Note Pads to specific roles, members, and text channels.
        - Reminders (Under construction)
    - Discord Games (Played on Discord chats)
        - Cards Against Humanity (Under construction).
            - Official decks up to Expansion 3.
            - In-sever Custom Decks.
        - "Secrete Message" (Under construction).
    - Novelty/misc
        - Self-destruct messages.
        - List all guilds hosting a Weebot.
        - Ping (Pong)


----
<a name='Roadmap'></a>
**Roadmap**
- Convert it all to Kotlin, oof
    1. (new) VoiceChannel temp mentionable Roles
    2. (transfer) Restrictions
    3. In-chat Reddit sim
    2. Custom commands/responses (regex or prefix)
    2. (transfer) Notepads
 - ~~Finish current basic settings implementations~~
 	- ~~Finish settings methods~~
 	- ~~Add User-joined Greetings~~
 - ~~Create Database to store information about each server using GSON/JSON~~
	- ~~Perhaps a Database package with proposéd classes as:~~
		- ~~*Database.java* Keeps a list of all things to be written to
		file (ie. a Databse object saved with gson)~~
		- ~~*Util* Package containing helper classes that build and
		update the database~~
			- ~~*DatabaseWriter.java*~~
			- ~~*DatabaseReader.java*~~
- ~~Make a NotePat/StickyNote functionality.~~
 - Make *Joke/Insult Generator*(TM)
	- Possibly using a pool of joke templates and drawing from a pool of random words and phrases
 -  Make *Cards against NL*
	- Send private messages to each player with their decks, send the white-card into the chat.
 - Research chat-bot implementations for more natural conversations
	 - Possibly add a *sass-scale*(TM) to determine the level of jokes/insults/casual form with specific members.
		 - Will need to track stats and information about each individual member and define what acts increase or decrease relationship status with Weebot.
		 - ? Should User relation to Weebot be gloabal or local
		* ? This would likelly invole a User wrapper class to keep track of the relationship
		* ? Though that would probably be needed anyway if we are to implement the
		* ? User-bot good/bad relationship meter thing (which I really wannt to)
- Fun Features (<>funhouse)
	- Russian Roulette Kick
	- Russian Roulette ~~Porn~~ DM (NSFW)
