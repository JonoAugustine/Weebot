/*
 * Copyright Aquatic Mastery Productions (c) 2020.
 */

package com.ampro.weebot.buildSrc

object PluginVersion {
    const val kotlin = "1.4.10"
    const val kapt = "1.4.10"
    const val serialization = "1.4.10"
    const val shadow = "6.0.0"
}

object DepVersion {
    const val serialization = "1.0.0"
    const val reflect = "1.4.10"
    const val coroutines = "1.4.0-M1"
    const val datetime = "0.1.0"
    const val strife = "dev-" + "e9f35f7c"
    const val logkat = "0.6.0"
    const val kmongo = "4.1.3"
}

fun kotlinx(module: String, version: String) =
    "org.jetbrains.kotlinx:kotlinx-$module:$version"

fun ktor(module: String, version: String) = "io.ktor:ktor-$module:$version"

fun strife(module: String, version: String) = "com.serebit.strife:strife-$module:$version"
