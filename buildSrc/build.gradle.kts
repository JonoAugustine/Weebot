/*
 * Copyright Aquatic Mastery Productions (c) 2020.
 */

plugins {
    `kotlin-dsl`
}

repositories {
    jcenter()
}
