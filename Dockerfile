# BUILD
FROM gradle AS BUILD

COPY . /setup
WORKDIR /setup
RUN gradle bot:shadowJar

# RUN

FROM gradle

COPY --from=BUILD /setup/bot/build/libs/bot-all.jar /app/bot.jar
WORKDIR /app

CMD ["java", "-jar", "bot.jar"]
