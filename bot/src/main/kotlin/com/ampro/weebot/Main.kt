/*
 * Copyright Aquatic Mastery Productions (c) 2019.
 */

package com.ampro.weebot

import com.ampro.weebot.bot.*
import com.ampro.weebot.bot.commands.*
import com.serebit.logkat.LogLevel
import com.serebit.logkat.Logger
import com.serebit.logkat.info
import com.serebit.logkat.trace
import com.serebit.logkat.writers.ConsoleWriter
import com.serebit.logkat.writers.MultiWriter
import com.serebit.strife.bot
import com.serebit.strife.data.Activity.Type.*
import com.serebit.strife.data.OnlineStatus
import com.serebit.strife.onReady
import kotlinx.coroutines.runBlocking
import kotlinx.datetime.Clock
import kotlin.concurrent.fixedRateTimer
import kotlin.time.ExperimentalTime

val logger = Logger().apply {
    level = LogLevel.TRACE
    writer = MultiWriter(ConsoleWriter())
}

object ENV {
    var token = System.getenv("BOT_TOKEN")
    var MONGODB_URI = "mongodb://mongo_db:27017/weebot?readPreference=primary&ssl=false"
}

/** Presences to cycle through during runtime */
val presences = listOf(
    Watching to "you",
    Listening to "your thoughts",
    Competing to "minecraft",
    Competing to "bible study",
    Playing to "with your heart"
)

@ExperimentalTime
suspend fun main(args: Array<String>) {
    if (ENV.token == null && args.isNotEmpty()) ENV.token = args[0]
    if (args.size > 1) ENV.MONGODB_URI = args[1]

    logger.info("Weebot Initializing")

    bot(ENV.token) {
        logToConsole = true // args.any { it == "--log" }

        install(WeebotMemoryAddon())
        install(ListenerAddon())
        install(CommandAddon())

        CommandAddon use Help and About and Ping
        CommandAddon use SuggestionCmd and OutHouseCmd and RegexTest and Settings
        CommandAddon use Shutdown and ToggleEnable and Statistics

        // TODO Refactor passives to fit Listener addon
        passives()

        onReady {
            Weebot.name = context.selfUser.getUsername()
            Weebot.uid = context.selfUser.id

            logger.info(
                """
                Weebot Initialized
                Version: ${Weebot.version}
                Target: ${Weebot.name} 
                """.trimIndent()
            )

            fixedRateTimer("presence updater", period = 5 * 60 * 1000) {
                logger.trace("Updating Presence")
                runBlocking {
                    context.updatePresence(
                        OnlineStatus.ONLINE,
                        since = Clock.System.now(),
                        activity = presences.random()
                    )
                }
            }
        }
    }
}
