/*
 * Copyright Jonathan Augustine (c) 2020.
 */

package com.ampro.weebot.db

import com.github.benmanes.caffeine.cache.Caffeine
import com.github.benmanes.caffeine.cache.LoadingCache
import kotlinx.coroutines.*
import java.util.concurrent.TimeUnit

/** A builder class used in the [cache] DSL */
class CacheBuilder<K, V> {
    /** Max size of the cache before old values are evicted */
    var maxSize: Long = 500

    /** Number of minutes after a write operation to evict the value */
    var expireAfterWrite: Long? = null

    /** Number of minutes after the last read operation to evict the value */
    var expireAfterAccess: Long? = null

    /** Lambda to run when values are evicted */
    private lateinit var onRemove: suspend (K, V) -> Unit

    /** Lambda to run when new values are requested */
    private lateinit var onGet: suspend (K) -> V?

    /** Set the lambda to run when values are evicted */
    fun onRemove(onRemove: suspend (K, V) -> Unit) {
        this.onRemove = onRemove
    }

    /** Set the lambda to run when new values are requested */
    fun onGet(onGet: suspend (K) -> V?) {
        this.onGet = onGet
    }

    /** Construct the cache */
    fun build(): LoadingCache<K, V> =
        Caffeine.newBuilder().recordStats().apply {
            maximumSize(maxSize)

            if (expireAfterWrite != null) {
                expireAfterWrite(expireAfterWrite!!, TimeUnit.MINUTES)
            }

            if (expireAfterAccess != null) {
                expireAfterAccess(expireAfterAccess!!, TimeUnit.MINUTES)
            }

            removalListener<K, V> { key, value, cause ->
                if (key != null && value != null && cause.wasEvicted()) {
                    GlobalScope.launch {
                        withContext(Dispatchers.Default) {
                            onRemove(key, value)
                        }
                    }
                }
            }
        }.build { k ->
            runBlocking { onGet(k) }
        }

}

/** Generate a new Caffeine Cache */
fun <K, V> cache(builder: CacheBuilder<K, V>.() -> Unit) = CacheBuilder<K, V>().apply(builder).build()
