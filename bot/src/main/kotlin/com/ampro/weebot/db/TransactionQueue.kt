/*
 * Copyright Jonathan Augustine (c) 2020.
 */

package com.ampro.weebot.db

import com.ampro.weebot.logger
import com.ampro.weebot.util.removeIf
import com.serebit.logkat.error
import com.serebit.logkat.trace
import kotlinx.coroutines.runBlocking
import org.litote.kmongo.`in`
import org.litote.kmongo.coroutine.CoroutineCollection
import kotlin.concurrent.fixedRateTimer
import kotlin.reflect.KProperty1

sealed class TransactionQueue<T : Any>(
    collection: CoroutineCollection<T>,
    private val id: KProperty1<T, Any>,
    val name: String = collection.namespace.collectionName,
    private val transaction: suspend TransactionQueue<T>.(MutableMap<String, T?>) -> Unit
) {

    private val Q = mutableMapOf<String, T?>()

    /** Add a new value to the queue */
    fun enqueue(key: Any, value: T? = null) {
        Q[key.toString()] = value
    }

    private val timer = fixedRateTimer(name, true, 0, 5 * 60 * 1000) {
        if (Q.isEmpty()) return@fixedRateTimer

        runBlocking {
            transaction(this@TransactionQueue, Q)
        }
    }
}

class SaveQueue<T : Any>(collection: CoroutineCollection<T>, id: KProperty1<T, Any>) :
    TransactionQueue<T>(collection, id, "${collection.namespace.collectionName}_SAVE", transaction = { tq ->
        logger.trace("Saving values to ${collection.namespace.collectionName}")

        val saveResult = tq
            .mapValues { (_, v) -> collection.save(v!!) }
            .mapValues { it.value?.wasAcknowledged() == true }

        val saveCount = saveResult.count { it.value }
        val failedCount = saveResult.size - saveCount

        tq.removeIf { k, _ -> saveResult[k] != true }

        when {
            saveCount > 0 -> logger.trace("Successfully saved $saveCount/${saveResult.size} from $name")
            failedCount > 0 -> logger.error("Failed to save $failedCount/${saveResult.size} from $name.")
        }
    })

class DeletionQueue<T : Any>(collection: CoroutineCollection<T>, id: KProperty1<T, Any>) :
    TransactionQueue<T>(collection, id, "${collection.namespace.collectionName}_DELETE", transaction = { tq ->
        if (collection.deleteMany(id `in` tq.keys).wasAcknowledged()) {
            logger.error("Successfully deleted bot queue.")
            tq.clear()
        } else logger.error("Failed to delete bot queue.")
    })