/*
 * Copyright Jonathan Augustine (c) 2020.
 */

package com.ampro.weebot.db

import com.ampro.weebot.ENV
import com.ampro.weebot.bot.Weebot
import com.ampro.weebot.bot.commands.Reddicord
import com.ampro.weebot.bot.commands.Suggestion
import com.ampro.weebot.stats.Statistic
import org.litote.kmongo.coroutine.coroutine
import org.litote.kmongo.reactivestreams.KMongo


/** KMongo Database client */
private val mongo by lazy {
    KMongo.createClient(ENV.MONGODB_URI)
        .coroutine
        .getDatabase(Weebot.name.toLowerCase())
}

/** Weebot Collections */
val weebots by lazy { mongo.getCollection<Weebot>("weebots") }

/** Reddicord Collection */
val reddicord by lazy { mongo.getCollection<Reddicord.Post>("reddicord") }

/** Suggestions Collection */
val suggestions by lazy { mongo.getCollection<Suggestion>("suggestions") }

/** Statistics Collection */
val statistics by lazy { mongo.getCollection<Statistic>("statistics") }

/** Cache of Weebots */
val weebotCache = cache<Long, Weebot> {
    expireAfterAccess = 10
    onRemove { _, bot -> weebots.save(bot) }
    onGet { id ->
        weebots.findOneById(id.toString())
            ?: Weebot(id).also { weebots.save(it) }
    }
}

/** Cache of Reddicord Posts */
val reddicordCache = cache<Long, Reddicord.Post> {
    expireAfterWrite = 2
    onRemove { _, post -> reddicord.save(post) }
    onGet { id -> reddicord.findOneById(id) }
}

/** Cache of Suggestions */
val suggestionCache = cache<String, Suggestion> {
    expireAfterWrite = 2
    expireAfterAccess = 3
    onRemove { _, post -> suggestions.save(post) }
    onGet { id -> suggestions.findOneById(id) }
}

/** Cache of Reddicord Posts */
val statisticCache = cache<String, Statistic> {
    expireAfterWrite = 2
    expireAfterAccess = 1
    onRemove { _, post -> statistics.save(post) }
    onGet { id -> statistics.findOneById(id) }
}

/** Bot saving Queue */
val weebotSaveQueue = SaveQueue(weebots, Weebot::guildID)

/** Bot delete Queue */
val weebotDeleteQueue = DeletionQueue(weebots, Weebot::guildID)

/** Reddicord Post saving Queue */
val reddicordSaveQueue = SaveQueue(reddicord, Reddicord.Post::guildID)

/** Reddicord Post delete Queue */
val reddicordDeleteQueue = DeletionQueue(reddicord, Reddicord.Post::guildID)

/** Suggestion saving Queue */
val suggestionSaveQueue = SaveQueue(suggestions, Suggestion::_id)

/** Suggestion delete Queue */
val suggestionDeleteQueue = DeletionQueue(suggestions, Suggestion::_id)

/** Statistic saving Queue */
val statisticSaveQueue = SaveQueue(statistics, Statistic::commandName)

/** Statistic delete Queue */
val statisticDeleteQueue = DeletionQueue(statistics, Statistic::commandName)

