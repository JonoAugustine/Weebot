/*
 * Copyright Jonathan Augustine (c) 2020.
 */

package com.ampro.weebot.db

import com.ampro.weebot.bot.Weebot
import com.ampro.weebot.bot.commands.Suggestion
import com.ampro.weebot.stats.Statistic
import kotlinx.datetime.Instant
import org.litote.kmongo.`in`
import org.litote.kmongo.descending
import org.litote.kmongo.gt

////////////////
// Weebot DSL //
////////////////

/** The "global" weebot */
val Weebot.Info.global get() = weebotCache[0]!!

/** Returns the weebot of the associated [id] */
fun getWeebot(id: Long) = weebotCache[id]

/** Queue the bot to be saved */
fun Weebot.save() = weebotSaveQueue.enqueue(guildID, this)

/** Queue the bot to be deleted */
fun Weebot.delete() = weebotDeleteQueue.enqueue(guildID)

/** Returns the estimated number of saved Weebots */
suspend fun estimateWeebotCount() = weebots.estimatedDocumentCount()

/////////////////////
// Suggestion DSL //
////////////////////

/** Returns the suggestion of the given [id] or null if not found */
fun getSuggestion(id: String) = suggestionCache[id]

/** Returns [limit] number of suggestions of the given [states] */
suspend fun getSuggestions(states: Iterable<Suggestion.State>, limit: Int = 100): List<Suggestion> =
    suggestions.find(Suggestion::state `in` states).limit(limit).toList()

// TODO Instant-based query

/** Queue the suggestion to be saved */
fun Suggestion.save() = suggestionSaveQueue.enqueue(_id, this)

/** Queue the suggestion to be deleted */
fun Suggestion.delete() = suggestionDeleteQueue.enqueue(_id)

/**
 * Returns [limit] number of suggestions which come after the [given time][after]
 *
 * @param limit Max number of results. Defaults to `100`
 */
suspend fun getSuggestions(after: Instant, limit: Int = 100) =
    suggestions.find(Suggestion::submitTime gt after)
        .limit(limit)
        .sort(descending(Suggestion::submitTime))
        .toList()
        .also { suggestionCache.putAll(it.associateBy { s -> s._id }) }

/** Returns the estimated number of suggestions */
suspend fun estimatedSuggestionCount() = suggestions.estimatedDocumentCount()

/////////////////////
// Statistics DSL //
////////////////////

/** Returns the statistic of the given [command][commandName] or null if not found */
fun getStatistic(commandName: String) = statisticCache[commandName]

/** Queue the statistic to be saved */
fun Statistic.save() = statisticSaveQueue.enqueue(commandName, this)

/** Queue the statistic to be deleted */
fun Statistic.delete() = statisticDeleteQueue.enqueue(commandName)