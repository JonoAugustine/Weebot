/*
 * Copyright Aquatic Mastery Productions (c) 2020.
 */

package com.ampro.weebot.util

import kotlinx.datetime.*

val Instant.isAfterNow: Boolean
    get() = this.until(
        Clock.System.now(),
        DateTimeUnit.MILLISECOND,
        TimeZone.currentSystemDefault()
    ) < 0

val Instant.isBeforeNow: Boolean
    get() = this.until(
        Clock.System.now(),
        DateTimeUnit.MILLISECOND,
        TimeZone.currentSystemDefault()
    ) > 0
