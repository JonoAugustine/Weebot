/*
 * Copyright Aquatic Mastery Productions (c) 2019.
 */

package com.ampro.weebot.stats

import kotlinx.serialization.Serializable
import org.bson.codecs.pojo.annotations.BsonId

// TODO Account for DMs and Guilds
/**
 * An tracker for a command's invocation usage.
 *
 * @property invokation The alias used to invoke the command
 * @property avgGuildSize The average size of the guild
 * @property count The number of times the [invokation] has been used
 */
@Serializable
data class UsageInfo(val invokation: String, var avgGuildSize: Int = 0, var count: Int = 0)

/**
 * A Command's statistic information
 *
 * @property commandName The command this statistic is for
 * @property usage A map of [UsageInfo] for this command
 */
@Serializable
data class Statistic(@BsonId val commandName: String, val usage: MutableMap<String, UsageInfo> = mutableMapOf())

/** The total number of invocations */
val Statistic.totalUsage: Int get() = usage.values.sumBy { it.count }

/** The average guild size across all usage information */
val Statistic.totalAvgGuildSize: Int get() = usage.values.sumBy { it.avgGuildSize } / usage.size

