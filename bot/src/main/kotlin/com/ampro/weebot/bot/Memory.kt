/*
 * Copyright Aquatic Mastery Productions (c) 2020.
 */

package com.ampro.weebot.bot

import com.ampro.weebot.db.delete
import com.ampro.weebot.db.getWeebot
import com.ampro.weebot.db.save
import com.ampro.weebot.db.weebotSaveQueue
import com.serebit.strife.BotAddonProvider
import com.serebit.strife.BotBuilder
import com.serebit.strife.botmemory.MemoryAddon
import com.serebit.strife.botmemory.MemoryProvider
import com.serebit.strife.onGuildCreate
import com.serebit.strife.onGuildDelete

object WeebotMemoryAddon : MemoryAddon<Weebot>() {

    override val memories: Map<Long, Weebot>
        get() = throw Exception()

    override val provider: MemoryProvider<Weebot> =
        MemoryProvider<Weebot>()
            .apply {
                guild {
                    Weebot(it.id)
                        .apply { weebotSaveQueue.enqueue(guildID, this) }
                }
            }

    override suspend fun getMemory(key: Long): Weebot? = getWeebot(key)

    override suspend fun forget(key: Long): Weebot? = getMemory(key)?.apply { delete() }

    override suspend fun modifyMemory(key: Long, scope: suspend Weebot.() -> Unit): Weebot? =
        getMemory(key)
            ?.also { scope(it) }
            ?.also { it.save() }

    override suspend fun remember(key: Long, memory: Weebot): Weebot = TODO()

    override fun installTo(scope: BotBuilder) = scope.run {
        onGuildCreate {
            if (getMemory(guild.id) == null) provider.guild!!(guild)
        }

        onGuildDelete {
            forget(guildID)
        }
    }

    operator fun invoke() = object : BotAddonProvider<WeebotMemoryAddon> {
        override fun provide() = WeebotMemoryAddon
    }
}

