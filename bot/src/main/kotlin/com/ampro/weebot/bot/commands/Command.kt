/*
 * Copyright Aquatic Mastery Productions (c) 2019.
 */


package com.ampro.weebot.bot.commands


import com.ampro.weebot.bot.Weebot
import com.ampro.weebot.bot.getArgs
import com.ampro.weebot.db.getStatistic
import com.ampro.weebot.db.save
import com.ampro.weebot.logger
import com.ampro.weebot.stats.Statistic
import com.ampro.weebot.stats.UsageInfo
import com.ampro.weebot.util.Regecies
import com.ampro.weebot.util.and
import com.ampro.weebot.util.isAfterNow
import com.ampro.weebot.util.isBeforeNow
import com.serebit.logkat.trace
import com.serebit.strife.entities.EmbedBuilder
import com.serebit.strife.entities.getGuild
import com.serebit.strife.events.MessageCreateEvent
import kotlinx.datetime.*

// TODO: Better command restrictions

/** Alias for command prefixes. */
typealias Prefix = String

data class Param(
    val name: String,
    val optional: Boolean = false
) {
    override fun toString(): String = buildString {
        if (optional) append('[') else append('<')
        append(name)
        if (optional) append(']') else append('>')
    }
}

fun listOfParams(vararg params: Any): List<Param> = params.map {
    when (it) {
        is Pair<*, *> -> Param(it.first as String, it.second as Boolean)
        else -> Param(it as String)
    }
}

data class Invokation(private val name: String, private val params: List<Param>) {
    override fun toString(): String = "${name.toLowerCase()} ${params.joinToString(" ")}"
}

infix fun String.with(list: List<Param>) = Invokation(this, list)

/**
 * @property rateLimit Number of seconds to wait after use.
 * @property details Additional information on the command.
 */
abstract class Command(
    val name: String,
    val aliases: List<String> = emptyList(),
    val children: List<Command> = emptyList(),
    var enabled: Boolean = true,
    val rateLimit: Int = 0,
    val guildOnly: Boolean = false,
    val devOnly: Boolean = false,
    val details: String,
    val params: List<Param> = emptyList(),
    val predicate: suspend MessageCreateEvent.(Weebot) -> Boolean = { true },
    val action: suspend MessageCreateEvent.(Weebot) -> Any?
) {

    /** Map of rate limits for this command. Maps userID to the next available usage time. */
    protected val rateLimitMap = mutableMapOf<Long, Instant>()

    /** Command Invocation definition */
    open val invokation: Invokation = name with params

    /** Predefined help information formatted in an [EmbedBuilder.FieldBuilder] */
    open val help: EmbedBuilder.FieldBuilder = EmbedBuilder.FieldBuilder(
        name, buildString {
            append('`').append(invokation).append("`\n")
            append(details).append("\nAliases: ")
            append(aliases.joinToString())
        }
    )

    /**
     * Static checks run to confirm valid invocation.
     * This is separate from the instance-dependent [predicate].
     */
    open suspend fun preCheck(event: MessageCreateEvent, bot: Weebot, arg: Int = 0): Boolean {
        val m = event.message
        val a = m.getAuthor() ?: return false
        return when {
            m.getArgs().size - arg < params.filterNot { it.optional }.size -> false
            a.isBot() -> false
            a.id in Weebot.devIDs -> true
            devOnly && a.id !in Weebot.devIDs -> false
            guildOnly && m.getGuild() == null -> false
            rateLimitMap[a.id]?.isAfterNow == true -> false
            rateLimitMap[a.id]?.isBeforeNow == true -> {
                rateLimitMap.remove(a.id)
                true
            }
            else -> m.getGuild()?.let {
                bot.cmdSettings[name]
                    ?.check(m.getGuild()!!.getMember(a.id))
                    ?: true
            } ?: true
        }
    }

    /**
     * Runs the command or child command. Runs [preCheck], [predicate], [action], and [postRun]
     *
     * @param arg The index of the invoking arg
     */
    open suspend fun run(event: MessageCreateEvent, bot: Weebot, arg: Int = 0) {
        // check child commands if there could be additional commands
        if (event.message.getArgs().size > arg + 1) {
            children
                .firstOrNull { "" and event.message.getArgs()[1] invokes it }
                ?.apply { return run(event, bot, arg + 1) }
        }

        logger.trace("Command $name invoked")
        when {
            // Run pre-checks
            !preCheck(event, bot, arg) -> return
            // Run predicate
            !predicate(event, bot) -> return
            // Run command
            // Run post
            else -> {
                action(event, bot)
                // Run post
                postRun(event, bot, arg)
            }
        }
    }

    /**
     * Performs general actions after the [command is run][run]: updates statistics and ratelimit.
     */
    open suspend fun postRun(event: MessageCreateEvent, bot: Weebot, arg: Int = 0) {
        if (rateLimit > 0) {
            rateLimitMap[event.message.getAuthor()!!.id] =
                Clock.System.now()
                    .plus(rateLimit, DateTimeUnit.SECOND, TimeZone.currentSystemDefault())
        }

        val guild = event.message.getGuild()

        (getStatistic(name) ?: Statistic(name))
            .let { stat ->
                val invoke = event.message.getArgs()[arg]
                stat.usage.getOrPut(invoke, { UsageInfo(invoke) })
                    .run {
                        count++
                        if (guild != null) {
                            // TODO Account for DMs and Guilds
                            avgGuildSize = (avgGuildSize + guild.getMembers().size) / count
                        }
                    }
                stat.save()
            }
    }
}

/**
 * Prefix + Arg invokes command?
 *
 * @param command
 * @return
 */
private infix fun Pair<Prefix, String>.invokes(command: Command): Boolean {
    val names = buildString {
        append('(').append(command.name)
        if (command.aliases.isNotEmpty())
            append('|').append(command.aliases.joinToString("|"))
        append(')')
    }
    return second.matches(Regex("${Regecies.ic}$first${names}(\\s+.*)?"))
}
