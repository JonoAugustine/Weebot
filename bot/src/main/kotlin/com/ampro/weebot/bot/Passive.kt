/*
 * Copyright Aquatic Mastery Productions (c) 2019.
 */

package com.ampro.weebot.bot

import com.ampro.weebot.db.getWeebot
import com.ampro.weebot.db.global
import com.serebit.strife.BotBuilder
import com.serebit.strife.botmemory.memory
import com.serebit.strife.entities.Guild
import com.serebit.strife.entities.User
import com.serebit.strife.events.Event
import com.serebit.strife.events.GuildEvent
import com.serebit.strife.onAnyEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.withContext

interface Passive {
    /**
     * Whether this Passive is currently active
     */
    var active: Boolean

    /**
     * Checks whether the given event and bot are valid invocations of this
     * Passive.
     *
     * Defaults to always true.
     */
    suspend fun predicate(event: Event, bot: Weebot): Boolean = true

    /**
     * Consumes the given [event] and [bot] to execute this Passive's action
     */
    suspend fun consume(event: Event, bot: Weebot)
}

/** Map of GuildID or User to passives. */
val passives = mutableMapOf<Long, MutableList<Passive>>()

fun <P : Passive> addPassive(id: Long, passive: P) {
    passives.getOrPut(id, { mutableListOf() }).add(passive)
}

inline fun <reified P : Passive> getPassives(id: Long): List<P>? =
    passives[id]?.filterIsInstance<P>()

fun <P : Passive> Guild.add(passive: P) {
    addPassive(id, passive)
    getWeebot(id)?.passives?.add(passive)
}

fun <P : Passive> User.add(passive: P) {
    addPassive(id, passive)
    addPassive(0, passive)
}

inline fun <reified P : Passive> Guild.getAll() = getPassives<P>(id)
inline fun <reified P : Passive> User.getAll() = getPassives<P>(id)

fun BotBuilder.passives() {
    onAnyEvent {
        withContext(Dispatchers.Default) {
            passives[0]?.retainAll { it.active }
            passives[0]
                ?.filter { it.active }
                ?.asFlow()
                ?.filter { it.predicate(this@onAnyEvent, Weebot.global) }
                ?.collect { it.consume(this@onAnyEvent, Weebot.global) }
        }
        if (this is GuildEvent) {
            passives.forEach { (id, list) ->
                withContext(Dispatchers.Default) {
                    list.retainAll { it.active }
                    list
                        .filter { it.active }
                        .asFlow()
                        .filter {
                            getWeebot(this@onAnyEvent.guild.id)
                                ?.let { bot -> it.predicate(this@onAnyEvent, bot) } == true
                        }
                        .collect { passive ->
                            memory<Weebot>(id) {
                                passive.consume(this@onAnyEvent, this)
                            }
                        }
                }
            }
        }
    }
}
