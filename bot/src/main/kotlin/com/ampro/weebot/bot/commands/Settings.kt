/*
 * Copyright Aquatic Mastery Productions (c) 2019.
 */

package com.ampro.weebot.bot.commands

import com.ampro.weebot.bot.getArgs
import com.ampro.weebot.bot.sendWEmbed
import com.serebit.strife.entities.getGuild
import com.serebit.strife.entities.inlineField
import com.serebit.strife.entities.reply
import com.serebit.strife.entities.title

object Settings : Command(
    "Settings",
    listOf("set"),
    children = listOf(Prefix),
    rateLimit = 30,
    details = "View and Change Weebot's settings.",
    action = {
        message.sendWEmbed {
            title("${message.getGuild()!!.getName()} Settings")
            inlineField("Prefix") { it.prefix }
        }
    }
) {

    object Prefix : Command(
        "Prefix",
        listOf("pref"),
        guildOnly = true,
        details = "Set my command prefix for this Server.",
        params = listOfParams("new_prefix"),
        rateLimit = 30,
        action = {
            it.prefix = message.getArgs()[1]
            message.reply("Prefix set to `${it.prefix}`")
        })
}

