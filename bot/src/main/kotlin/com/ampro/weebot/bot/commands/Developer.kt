/*
 * Copyright Aquatic Mastery Productions (c) 2019.
 */

package com.ampro.weebot.bot.commands


import com.ampro.weebot.bot.CommandAddon
import com.ampro.weebot.bot.Weebot
import com.ampro.weebot.bot.getArgs
import com.ampro.weebot.bot.sendWEmbed
import com.ampro.weebot.db.getStatistic
import com.ampro.weebot.stats.totalAvgGuildSize
import com.ampro.weebot.stats.totalUsage
import com.ampro.weebot.util.Regecies
import com.ampro.weebot.util.matchesAny
import com.serebit.strife.entities.field
import com.serebit.strife.entities.inlineField
import com.serebit.strife.entities.reply
import com.serebit.strife.entities.title
import com.serebit.strife.events.MessageCreateEvent
import kotlin.system.exitProcess

abstract class DeveloperCommand(
    name: String,
    aliases: List<String> = emptyList(),
    children: List<Command> = emptyList(),
    enabled: Boolean = true,
    details: String,
    params: List<Param> = emptyList(),
    predicate: suspend MessageCreateEvent.(Weebot) -> Boolean = { true },
    action: suspend MessageCreateEvent.(Weebot) -> Any?
) : Command(
    name,
    aliases,
    children,
    enabled,
    devOnly = true,
    details = details,
    params = params,
    predicate = predicate,
    action = action
)

object Shutdown : DeveloperCommand(
    "Shutdown",
    listOf("kill"),
    details = "Shutdown the bot.",
    action = {
        message.reply("I was kill")
        context.disconnect()
        exitProcess(0)
    }
)

/** Toggle a [Command.enabled] */
object ToggleEnable : DeveloperCommand(
    "toggle",
    listOf("tog"),
    details = "toggle the enable state of a command",
    params = listOfParams("command_name", "on/off"),
    action = {
        CommandAddon[message.getArgs()[1]]?.let {
            it.enabled = when {
                message.getArgs()[2].matchesAny(Regecies.on, Regecies.enable) -> true
                message.getArgs()[2].matchesAny(Regecies.off, Regecies.disable) -> {
                    false
                }
                else -> return@let message.reply("Invalid args")
            }
            message.reply(
                "${if (it.enabled) "en" else "dis"}abled `${it.name}`"
            )
        } ?: message.reply("No command found at `${message.getArgs()[1]}`.")
    }

)

object Statistics : DeveloperCommand(
    "statistics",
    listOf("stat", "stats"),
    details = "view statistics",
    params = listOfParams("command_name" to true),
    action = a@{
        val cmdName = message.getArgs().takeIf { it.size > 1 }?.get(1)

        if (cmdName != null) {
            val command = CommandAddon[cmdName]
                ?: return@a message.reply("No command found with name $cmdName")

            val statistic = getStatistic(command.name)
                ?: return@a message.reply("Failed to retrieve statistics for $cmdName (${command.name})")

            message.reply {
                title("${command.name} Statistics")
                field("Overall") {
                    """
                        Total Usage: ${statistic.totalUsage}
                        Total Avg Guild Size: ${statistic.totalAvgGuildSize}
                    """.trimIndent()
                }
                statistic.usage.entries
                    .sortedByDescending { it.value.count }
                    .forEach { (k, v) ->
                        inlineField(k) {
                            """
                                Usage: ${v.count}
                                Avg Guild Size: ${v.avgGuildSize}
                            """.trimIndent()
                        }
                    }
            }

        } else {
            val statistics = CommandAddon.getAll().mapNotNull { getStatistic(it.name) }

            message.sendWEmbed {
                title("General Statistics")
                description =
                    if (statistics.isNotEmpty()) {
                        statistics.joinToString("\n\n") {
                            """
                            Command Name: ${it.commandName}
                            Usage: ${it.totalUsage}
                            Avg Guild Size: ${it.totalAvgGuildSize}
                            """.trimIndent()
                        }
                    } else "No statistics available"
            }
        }
    }
)
