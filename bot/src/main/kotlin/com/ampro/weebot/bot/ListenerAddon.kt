/*
 * Copyright Aquatic Mastery Productions (c) 2020.
 */

package com.ampro.weebot.bot

import com.serebit.strife.BotAddon
import com.serebit.strife.BotAddonProvider
import com.serebit.strife.BotBuilder
import com.serebit.strife.events.Event
import com.serebit.strife.onAnyEvent
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.withContext
import kotlin.reflect.KClass

private const val NAME: String = "WEEBOT_LISTENER_ADDON"

/** Event Listener wrapper class */
data class Listener(
    /** The listener function */
    val block: suspend (Event) -> Boolean,
    /** Whether the listener should be removed or not */
    var alive: Boolean = true
) {
    /** Run the listener's code */
    suspend operator fun invoke(event: Event) {
        alive = block(event)
    }
}

/**
 * Adds an event listener for the given event types.
 *
 * @param targets The target event types
 * @param listener The listener
 */
fun BotBuilder.listenTo(
    vararg targets: KClass<out Event>,
    listener: suspend (Event) -> Boolean
) = (getAddon(NAME) as? ListenerAddon)?.listenTo(targets, Listener(listener))

/**
 * Addon which allows for adding & removing event listeners at runtime.
 *
 * @since 4.1.0
 */
object ListenerAddon : BotAddon {

    /**
     * Maps Events to lists of listeners. Listeners with multiple event targets
     * are added to multiple lists.
     */
    private val eventListenerMap =
        mutableMapOf<KClass<out Event>, MutableList<Listener>>()

    /** Add an event listener */
    fun listenTo(targets: Array<out KClass<out Event>>, listener: Listener) =
        targets.forEach {
            eventListenerMap.getOrPut(it, { mutableListOf() }).add(listener)
        }

    override fun installTo(scope: BotBuilder) {
        scope.onAnyEvent {
            val list = eventListenerMap[this::class] ?: return@onAnyEvent
            list.retainAll { it.alive }

            if (list.isNotEmpty()) {
                withContext(Dispatchers.Default) {
                    list.asFlow().collect {
                        it(this@onAnyEvent)
                    }
                }
            } else eventListenerMap.remove(this::class)
        }
    }

    override val name: String = NAME

    operator fun invoke() = object : BotAddonProvider<ListenerAddon> {
        override fun provide() = ListenerAddon
    }
}

