/*
 * Copyright Aquatic Mastery Productions (c) 2020.
 */

package com.ampro.weebot.bot

import com.ampro.weebot.bot.commands.Command
import com.ampro.weebot.logger
import com.serebit.logkat.trace
import com.serebit.strife.BotAddon
import com.serebit.strife.BotAddonProvider
import com.serebit.strife.BotBuilder
import com.serebit.strife.botmemory.memory
import com.serebit.strife.entities.getGuild
import com.serebit.strife.entities.isHuman
import com.serebit.strife.onMessageCreate

private const val NAME = "WEEBOT_COMMAND_ADDON"


object CommandAddon : BotAddon, Iterable<Pair<String, Command>> {

    private val _commands = mutableMapOf<String, Command>()

    val commands: Map<String, Command> get() = _commands.toMap()

    /** Returns the command associated with the given [name] or null. */
    fun getCommand(name: String) = _commands[name]

    /** Returns a list of all installed commands */
    fun getAll() = _commands.values.distinctBy { it.name }

    /** Registers the [command][c] with the [CommandAddon] */
    operator fun plus(c: Command) {
        (c.aliases + c.name)
            // normalize names
            .map { it.toLowerCase() }
            // Add each name/alias to command map
            .forEach {
                require(!_commands.containsKey(it)) {
                    "Command with name $it already registered."
                }
                _commands[it] = c
            }
        logger.trace("Registered command with name ${c.name}")
    }

    /** Registers the [command][c] with the [CommandAddon] */
    infix fun use(c: Command) = apply { this + c }

    /** Registers the [command][c] with the [CommandAddon] */
    infix fun and(c: Command) = apply { this + c }

    /** Returns the command associated with the given [name] or null. */
    operator fun get(name: String) = this.getCommand(name)

    override fun installTo(scope: BotBuilder) {
        scope.apply {
            onMessageCreate {
                if (message.getAuthor()?.isHuman() == true) {
                    memory<Weebot>(message.getGuild()?.id ?: 0) {
                        _commands[message.getContent().removePrefix(prefix).split(' ')[0]]
                            ?.run(this@onMessageCreate, this)
                    }
                }
            }
        }
    }

    /** Returns a provider */
    operator fun invoke(): BotAddonProvider<CommandAddon> =
        object : BotAddonProvider<CommandAddon> {
            override fun provide() = CommandAddon
        }

    override fun iterator() = this.commands.toList().iterator()

    override val name = NAME
}
