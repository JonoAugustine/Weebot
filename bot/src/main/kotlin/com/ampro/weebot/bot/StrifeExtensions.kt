/*
 * Copyright Aquatic Mastery Productions (c) 2020.
 */

package com.ampro.weebot.bot

import com.serebit.strife.BotBuilder
import com.serebit.strife.BotClient
import com.serebit.strife.StrifeInfo
import com.serebit.strife.data.Color
import com.serebit.strife.entities.*
import kotlinx.coroutines.delay
import kotlin.time.ExperimentalTime
import kotlin.time.seconds

infix fun User.`is`(id: Long) = this.id == id

suspend fun Message.getArgs() = getContent().split(' ')

/**
 * Get an [EmbedBuilder] with the [author][EmbedBuilder.author],
 * [footer][EmbedBuilder.footer], [color][EmbedBuilder.color] setup.
 *
 * @param builder Additional scope to modify the embed
 * @return The new [EmbedBuilder].
 */
suspend fun wEmbed(context: BotClient, builder: suspend EmbedBuilder.() -> Unit) = embed {
    color = Color.GREEN
    val un = context.selfUser.getUsername()
    author {
        name = un
        imgUrl = context.selfUser.getAvatar().uri
    }
    footer {
        text = "Run by $un with Strife"
        imgUrl = StrifeInfo.logoUri
    }
}.apply { builder(this) }

suspend fun Message.sendWEmbed(builder: suspend EmbedBuilder.() -> Unit): Message? =
    getChannel().sendWEmbed(builder)

suspend fun TextChannel.sendWEmbed(builder: suspend EmbedBuilder.() -> Unit) =
    send(wEmbed(context, builder))


@ExperimentalTime
suspend fun Message.delete(seconds: Int) = run {
    delay(seconds.seconds.toLongMilliseconds())
    delete()
}

////////////////////////////////////////////////////////////////////////////////

/** Returns the BotAddon with the associated name */
fun BotBuilder.getAddon(name: String) = addons[name]
