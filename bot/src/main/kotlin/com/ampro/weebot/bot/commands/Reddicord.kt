/*
 * Copyright Aquatic Mastery Productions (c) 2019.
 */

package com.ampro.weebot.bot.commands

import com.ampro.weebot.bot.*
import com.ampro.weebot.util.Regecies
import com.ampro.weebot.util.matchesAny
import com.ampro.weebot.util.unit
import com.serebit.strife.entities.*
import com.serebit.strife.events.Event
import com.serebit.strife.events.MessageCreateEvent
import com.serebit.strife.events.MessageReactionAddEvent
import com.serebit.strife.events.MessageReactionRemoveEvent
import com.serebit.strife.text.italic
import kotlinx.serialization.Serializable
import org.bson.codecs.pojo.annotations.BsonId

class Reddicord(var channelID: Long = -1) : Passive {

    @Serializable
    data class Post(
        @BsonId val messageID: Long,
        val guildID: Long,
        val authorID: Long,
        val authorName: String,
        var score: Int
    )

    private val posts = mutableMapOf<Long, MutableMap<Long, Int>>()

    override var active: Boolean = true

    override suspend fun consume(event: Event, bot: Weebot) {
        when (event) {
            is MessageCreateEvent -> TODO("Check Message")
            is MessageReactionAddEvent -> TODO("Modify Score")
            is MessageReactionRemoveEvent -> TODO("Modify Score")
        }
    }
}

object ReddicordCmd : Command(
    "Reddicord",
    listOf("rcord", "rcc"),
    listOf(LeaderBoard),
    details = TODO(),
    rateLimit = 30,
    guildOnly = true,
    params = listOfParams("on/off" to true, "channel" to true),
    predicate = {
        if (message.getArgs().size > 1)
            message.getArgs().size > 2 && message.getArgs()[1].matchesAny(
                Regecies.on,
                Regecies.off,
                Regecies.enable,
                Regecies.disable
            )
        else true
    },
    action = a@{
        if (message.getArgs().size == 1) {
            message.sendWEmbed {
                val rd = message.getGuild()!!.getAll<Reddicord>()?.firstOrNull()
                title(
                    "Reddicord is " + if (rd == null) "Disabled" else "Active"
                )
                description = "Use `reddicord on/off #channel` to (de)activate."
            }
        } else {
            if (message.getArgs()[1].matchesAny(Regecies.enable, Regecies.on)) {
                message.getGuild()!!.getAll<Reddicord>()
                    ?.takeIf { it.isNotEmpty() }
                    ?.run {
                        return@a message.reply("Reddicord is already active")
                            .unit
                    }
                message.mentionedChannels().firstOrNull()
                    ?.let { it as GuildTextChannel }
                    ?.also { message.getGuild()!!.add(Reddicord(it.id)) }
                    ?.run {
                        message.reply(
                            "Reddicord is now active in ${asMention()}.".italic
                        )
                    } ?: message.reply("Please mention a text channel as well.")
            } else {
                message.getGuild()!!.getAll<Reddicord>()
                    ?.firstOrNull()
                    ?.run {
                        active = false
                        message.reply("Reddicord deactivated")
                    }
            }
        }
    }
) {
    object LeaderBoard : Command(
        TODO(),
        TODO(),
        TODO(),
        TODO(),
        TODO(),
        TODO(),
        TODO(),
        TODO(),
        TODO(),
        TODO(),
        TODO()
    )
}
