/*
 * Copyright Aquatic Mastery Productions (c) 2018.
 */

package com.ampro.weebot.bot.commands

import com.ampro.weebot.bot.*
import com.ampro.weebot.logger
import com.ampro.weebot.util.contains
import com.ampro.weebot.util.removeAll
import com.serebit.logkat.trace
import com.serebit.strife.entities.*
import com.serebit.strife.events.Event
import com.serebit.strife.events.MessageCreateEvent
import com.serebit.strife.getUser
import com.serebit.strife.text.boldItalic
import com.serebit.strife.text.italic
import kotlinx.datetime.*
import kotlinx.serialization.SerialName
import kotlinx.serialization.Serializable
import kotlin.math.absoluteValue


/** An instantiable representation of a User's OutHouse. */
@Serializable
@SerialName("outhouse")
class OutHouse(val userID: Long, val note: String, val forwardMessages: Boolean, val minutes: Int) : Passive {

    private val startEpochSeconds = Clock.System.now().epochSeconds
    private val start: Instant
        get() = Instant.fromEpochSeconds(startEpochSeconds)

    internal val end: DateTimePeriod
        get() = start.periodUntil(
            start.plus(
                minutes,
                DateTimeUnit.MINUTE,
                TimeZone.currentSystemDefault()
            ),
            TimeZone.currentSystemDefault()
        )

    override var active: Boolean = true
        get() = field && end.minutes > 0

    override suspend fun predicate(event: Event, bot: Weebot): Boolean =
        event is MessageCreateEvent &&
            event.message.getAuthor()?.isHuman() == true &&
            event.message.getGuild() != null

    override suspend fun consume(event: Event, bot: Weebot) {
        event as MessageCreateEvent

        val guild = event.message.getGuild()!!
        val mess = event.message
        val author = event.message.getAuthor()!!

        logger.trace("OHC consume")

        if (author `is` userID) {
            guild.getMember(userID)?.run {
                author.getAll<OutHouse>()?.forEach { it.active = false }
                event.channel.send(
                    "Welcome back ${getNickname() ?: author.getUsername()}".italic
                )
            }
        } else if (mess.mentions(userID)) {
            //Respond as bot
            event.channel.send(
                buildString {
                    append("Sorry, ")
                    append(
                        guild.getMember(userID)
                            ?.run { getNickname() ?: getUser().getUsername() }
                            ?: "that user"
                    )
                    if (note.isNotBlank()) append(" is out $note. ")
                    else append(" is currently unavailable. ")
                    append("Please try mentioning them again in ")
                    end.run { append("$days days, $minutes minutes") }
                    append(". Thank you.")
                }.italic
            )

            //Forward Message
            if (forwardMessages) {
                val m = guild.getMember(event.message.getAuthor()!!.id)
                event.context.getUser(userID)?.createDmChannel()?.sendWEmbed {
                    title(
                        buildString {
                            append("Message From ")
                            append(event.message.getAuthor()!!.getUsername().boldItalic)
                            m?.run {
                                append(" (a.k.a ")
                                append(
                                    (m.getNickname() ?: m.getUser()
                                        .getUsername()).boldItalic
                                )
                                append(")")
                            }
                            append(" in ").append(guild.getName().boldItalic)
                        }
                    )
                    description = "\"${event.message.getContent()}\""
                }
            }
        }
    }

}

/**
 * Have the bot respond to your metions while you're AFK but shown as online.
 * Can also forward messages to a private channel.
 *
 * @author Jonathan Augustine
 * @since 1.0
 */
object OutHouseCmd : Command(
    "OutHouse",
    listOf("ohc"),
    rateLimit = 60,
    details = buildString {
        append("Have the bot respond to anyone who mentions you for the given ")
        append("time. You can have all mentions of you forwarded to a DM ")
        append("with the `-f` option.")
    },
    params = listOfParams("#d" to true, "#h" to true, "-f" to true, "activity" to true),
    action = {
        //ohc [hours] [note here]
        message.getAuthor()?.getAll<OutHouse>()
            ?.takeIf { it.isNotEmpty() }
            ?.apply {
                message.reply("You're already in the outhouse".italic)
            } ?: run {
            //Add new OH
            val args = message.getArgs()

            //check days
            val d = args.firstOrNull { it.matches("\\d+[Dd]".toRegex()) }
                ?.removeAll("[^\\d]+")?.toInt()?.absoluteValue ?: 0
            val h = args.firstOrNull { it.matches("\\d+[Hh]".toRegex()) }
                ?.removeAll("[^\\d]+")?.toInt()?.absoluteValue ?: 0

            val min = when {
                d == 0 && h == 0 -> 60
                else -> (d * 24 * 60) + (h * 60)
            }

            //check for forwarding
            val forward = args.contains("(?i)-f(orward(ing)?)?".toRegex())

            var messageIndex = 1
            if (d > 0) messageIndex++
            if (h > 0) messageIndex++
            if (forward) messageIndex++

            val message = if (args.size - 1 >= messageIndex) {
                args.subList(messageIndex, args.size).joinToString(" ")
            } else ""

            val oh = OutHouse(this.message.getAuthor()!!.id, message, forward, min)
            this.message.getAuthor()?.add(oh)
            this.message.reply(
                buildString {
                    append("I will hold down the fort while you're away")
                    if (message.isNotEmpty()) {
                        append(' ').append('"').append(message).append('"')
                    }
                    append(". ").append(UnicodeEmoji.WomanGuard)
                    append(" see you in ")
                    append("${oh.end.days}:${oh.end.hours}:${oh.end.minutes}")
                }
            )
        }
    }
)

