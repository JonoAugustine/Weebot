/*
 * Copyright Aquatic Mastery Productions (c) 2019.
 */

package com.ampro.weebot.bot.commands

import com.ampro.weebot.bot.CommandAddon
import com.ampro.weebot.bot.Weebot
import com.ampro.weebot.bot.getArgs
import com.ampro.weebot.bot.sendWEmbed
import com.serebit.strife.StrifeInfo
import com.serebit.strife.entities.*
import com.serebit.strife.entities.UnicodeEmoji.Companion.PingPong
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.filter
import kotlinx.coroutines.flow.filterNot
import kotlinx.coroutines.runBlocking

object Help : Command(
    name = "Help",
    rateLimit = 60,
    params = listOfParams("command_name" to true),
    details = "Get a list of all my commands or extra information about a particular command",
    action = action@{
        val author = message.getAuthor() ?: return@action Unit
        val args = message.getArgs()

        if (args.size == 1) {
            author.createDmChannel()?.sendWEmbed {
                title("${Weebot.name} Commands")
                CommandAddon.commands.values
                    .distinct()
                    .asFlow()
                    .filterNot { it.devOnly && author.id !in Weebot.devIDs }
                    .filter { it.enabled }
                    .collect { fields.add(it.help) }
            }
        } else {
            CommandAddon.commands[args[1]]
                ?.takeIf { it.devOnly && author.id !in Weebot.devIDs }
                ?.let { command ->
                    author.createDmChannel()
                        ?.sendWEmbed {
                            title("${command.name} Help")
                            fields.add(command.help)
                            field("Child Commands") {
                                "The commands are preceded by their parent"
                            }
                            command.children.forEach { child ->
                                fields.add(child.help.apply { inline = true })
                            }
                        }
                } ?: message.reply("I do not have a command called ${args[1]}.")
        }
        message.delete()
    }
)

object About : Command(
    "About",
    children = listOf(Me),
    details = "Get cool info about me or you!",
    params = listOfParams("me" to true),
    action = {
        message.sendWEmbed {
            title("About ${Weebot.name}")
            description = "I was made by [Jono](${Weebot.jono}) " +
                    "using [Kotlin](https://kotlinlang.org) and " +
                    "[Strife](${StrifeInfo.sourceUri}). I am currently running " +
                    "my version ${Weebot.version} build. I am currently in " +
                    "the alpha stage of ${Weebot.version}, so old features " +
                    "may not have been remade yet. Thank you for your patience!."
        }
    }
) {

    object Me : Command(
        "Me",
        details = "Get information about yourself.",
        action = {
            val user = message.getAuthor()!!
            val member = message.getGuild()?.getMember(user.id)
            val username = user.getUsername()
            val activeName = member?.getNickname() ?: user.getUsername()
            val uid = user.id
            val created = user.createdAt
            val joined = member?.getJoinedAt()
            val roles = member?.getRoles()
            val isOwner = member?.getGuild()?.getOwner()?.getUser()?.id == user.id
            val presence = member?.getPresence()

            message.sendWEmbed {
                title(activeName)
                thumbnail(user.getAvatar().uri)
                description = """
                    username: $username
                    nickname: $activeName
                    ID: $uid
                """.trimIndent()

                inlineField("Joined Discord") {
                    created.toString()
                }

                joined?.let {
                    inlineField("Joined ${member.getGuild().getName()}") {
                        joined.toString()
                    }
                    inlineField("Owner of ${member.getGuild().getName()}?") {
                        if (isOwner) "Yes" else "No"
                    }
                    presence?.let {
                        inlineField("Status") {
                            buildString {
                                it.clientStatus.run {
                                    append("Desktop: ").append(desktop)
                                    append('\n')
                                    append("Mobile: ").append(mobile)
                                    append('\n')
                                    append("Web: ").append(web)
                                    append('\n')
                                }
                                it.game?.run {
                                    append(type.name)
                                    append(" ").append(activeName)
                                    timespan?.start?.run {
                                        append(" since ")
                                        append(this.toString())
                                        append('\n')
                                    }
                                    url?.run(this@buildString::append)
                                }
                            }
                        }
                    }
                    if (roles?.isNotEmpty() == true) {
                        inlineField("Roles") {
                            roles.joinToString {
                                runBlocking { it.getName() }
                            }
                        }
                    }
                    color = member.getHighestRole()?.getColor() ?: color
                }
            }
        }
    )
}

object Ping : Command(
    "ping",
    listOf("pong", "latency"),
    details = "Check my response times",
    rateLimit = 30,
    action = { message.reply("Pong! $PingPong ${context.gatewayLatency}ms") }
)
