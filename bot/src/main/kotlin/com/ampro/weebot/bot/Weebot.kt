package com.ampro.weebot.bot

import com.ampro.weebot.bot.commands.GateKeeper
import com.ampro.weebot.bot.commands.Reddicord
import com.ampro.weebot.bot.commands.VCRoleManager
import com.serebit.strife.BotClient
import com.serebit.strife.botmemory.Memory
import com.serebit.strife.botmemory.MemoryType
import com.serebit.strife.data.Permission
import com.serebit.strife.entities.GuildMember
import com.serebit.strife.getGuild
import kotlinx.serialization.Contextual
import kotlinx.serialization.Serializable
import org.bson.codecs.pojo.annotations.BsonId

/*
 * Copyright Aquatic Mastery Productions (c) 2019.
 */

suspend fun Weebot.guild(context: BotClient) = context.getGuild(guildID)

@Serializable
data class Weebot(
    @BsonId val guildID: Long,
    var prefix: String = defaultPrefix,
    val cmdSettings: MutableMap<String, @Contextual CommandSettings> = mutableMapOf()
) : Memory {

    override val type: MemoryType = MemoryType.Guild

    val passiveList = mutableListOf<Passive>()

    @Contextual
    val passives: Passives = Passives()

    inner class Passives(
        var gateKeeper: GateKeeper? = null,
        var reddicord: Reddicord? = null,
        var vcRoleManager: VCRoleManager? = null
    ) {
        init {
            gateKeeper?.let { addPassive(guildID, it) }
            reddicord?.let { addPassive(guildID, it) }
            vcRoleManager?.let { addPassive(guildID, it) }
        }

        fun <P : Passive> add(p: P) {
            when (p) {
                is GateKeeper -> gateKeeper = p
                is Reddicord -> reddicord = p
                is VCRoleManager -> vcRoleManager = p
            }
        }
    }

    companion object Info {
        const val version = "4.2.1"
        const val jono = "https://JonoAugustine.com"
        const val defaultPrefix = ">"

        /** Weebot or Tobeew */
        lateinit var name: String
        var uid: Long = 0
        val devIDs = listOf(139167730237571072L, 186130584693637131L)
    }

}

@Serializable
data class CommandSettings(
    val permissions: MutableList<Permission>,
    val roles: MutableList<Long> = mutableListOf()
) {
    suspend fun check(guildMember: GuildMember?): Boolean = guildMember?.run {
        getRoles().any { it.id in this@CommandSettings.roles } ||
            permissions.any { it in this@CommandSettings.permissions }
    } ?: false
}
