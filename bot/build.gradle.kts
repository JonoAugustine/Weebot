/*
 * Copyright Aquatic Mastery Productions (c) 2020.
 */

import com.ampro.weebot.buildSrc.DepVersion
import com.ampro.weebot.buildSrc.kotlinx
import com.ampro.weebot.buildSrc.strife
import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    kotlin("jvm")
    kotlin("kapt")
    kotlin("plugin.serialization") apply true
    id("application")
    id("com.github.johnrengelman.shadow") version "6.0.0"
}

repositories {
    maven("https://jitpack.io")
    maven("http://jcenter.bintray.com")
    maven("https://gitlab.com/api/v4/projects/6502506/packages/maven")
    maven("https://dl.bintray.com/michaelbull/maven")
}

dependencies {
    // Kotlin
    implementation(kotlin("stdlib-jdk8"))
    implementation(kotlin("reflect", DepVersion.reflect))
    implementation(kotlinx("coroutines-core", DepVersion.coroutines))
    implementation(kotlinx("serialization-json", DepVersion.serialization))
    implementation(kotlinx("datetime", DepVersion.datetime))

    // Discord
    implementation(strife("client-jvm", DepVersion.strife))
    implementation(strife("addons-memory-jvm", DepVersion.strife))

    // Loggin
    implementation("com.serebit.logkat", "logkat", DepVersion.logkat)
    implementation("org.slf4j:slf4j-simple:1.6.1")

    // Utillity
    implementation("com.michael-bull.kotlin-retry:kotlin-retry:1.0.1")
    implementation("com.github.ben-manes.caffeine:caffeine:2.6.2")

    // TODO Remove this when API created
    // Mongo
    implementation("org.litote.kmongo", "kmongo-coroutine", DepVersion.kmongo)

    //APIs
    //implementation("com.twilio.sdk:twilio:7.9.0")
    //implementation("me.sargunvohra.lib:pokekotlin:2.3.0")
    //implementation("org.mariuszgromada.math:MathParser.org-mXparser:4.1.1")
    //implementation("ca.pjer:chatter-bot-api:1.4.7")

    // Testing
    //testImplementation("junit", "junit", "4.12")
}

tasks.withType<KotlinCompile>().configureEach {
    kotlinOptions {
        jvmTarget = "1.8"
    }
}

application {
    mainClassName = "${group}.weebot.MainKt"
}

configure<JavaPluginConvention> {
    sourceCompatibility = JavaVersion.VERSION_1_8
}
